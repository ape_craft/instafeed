//
//  IFMedia.h
//  InstaFeed
//
//  Created by Admin on 5/2/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstagramMedia.h"
#import "InstagramUser.h"
#import "InstagramComment.h"

@protocol IFMediaImagesLoadDelegate

- (void) loadedImageWithError:(NSError*) error;

@end

@interface IFMedia : NSObject

@property (strong, nonatomic) id<IFMediaImagesLoadDelegate> delegate;
@property (strong, nonatomic) InstagramMedia *media;
@property (strong, nonatomic) NSURL *profileURL;
@property (strong, nonatomic) NSURL *thumbnailURL;
@property (strong, nonatomic) NSMutableDictionary *mediaFiles;
@property (strong, nonatomic) NSArray *commentsArray;
@property (copy, nonatomic) NSString *comment;
@property (copy, nonatomic) NSString *username;
@property (copy, nonatomic) NSString *createdDate;
@property (nonatomic) NSInteger likesCount;
@property (nonatomic) NSInteger commentsCount;
@property (nonatomic) BOOL liked;

- (id) initWithInstagramMedia:(InstagramMedia *) media liked:(BOOL) liked;
- (void) updateLikedAction;
- (NSString *) getLikesCount;
- (NSString *) getCommentsCount;

@end
