//
//  IFFeedViewController.m
//  InstaFeed
//
//  Created by Admin on 5/1/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "IFFeedViewController.h"
#import "IFPhotoTableViewCell.h"
#import "IFMedia.h"
#import "IFMediaDownloadManager.h"
#import "IFUser.h"

@interface IFFeedViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, IFPhotoTableViewCellDelegate>

@property (strong, nonatomic) NSMutableArray *mediaArray;
@property (strong, nonatomic) IFUser *user;
@property (strong, nonatomic) NSIndexPath *visibleCellIndexPath;
@property (strong, nonatomic) NSMutableSet *nextMaxIdSet;
@property (nonatomic) FeedLoaderState feedLoader;

@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (strong, nonatomic) NSMutableDictionary* mediaImageInProgressDownloads;
@property (strong, nonatomic) InstagramPaginationInfo *currentPaginationInfo;

@end

@implementation IFFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// TO login as another user tap back bar button
// to be redirected to instagram login page

// initialize containers
- (void)viewDidLoad {
    [super viewDidLoad];
    _mediaArray = [[NSMutableArray alloc] init];
    _nextMaxIdSet = [[NSMutableSet alloc] init];
    _user = [[IFUser alloc] init];
    [self loadSelfFeed];
}

// Cancel all active downloads on memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    IFMediaDownloadManager* downloadManager = [IFMediaDownloadManager instance];
    [downloadManager cancelAllDownloads];
}

#pragma mark UITableViewDelegate
// Define default uitableviewcell height for different orientations
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IFMedia* media = [_mediaArray objectAtIndex:indexPath.row];
    
    CGFloat defaultHeight = 492.0;
    CGFloat cellWidth = 280;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation != UIDeviceOrientationPortrait) {
        cellWidth = 440;
        defaultHeight = 359;
    }
    // Portrait orientation
//    CGRect newFrame = CGRectMake(20, 431, 280, 21);
    
    //Landscape orientation
    // CGRectMake(20, 318, 440, 21);
    
    CGSize maximumLabelSize = CGSizeMake(cellWidth, FLT_MAX);
    UIFont *font = [UIFont systemFontOfSize:17.0];
    
    CGRect expectedLabelSize = [media.comment boundingRectWithSize:maximumLabelSize
                                options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{NSFontAttributeName: font}
                                context:nil];
    
    defaultHeight += expectedLabelSize.size.height - 21;
    return defaultHeight;
}

// Preload more recent feed till user bumps into a bottom of table

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row + 3 == _mediaArray.count) {
        [self loadSelfFeed];
        _feedLoader = FeedLoaderNull;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mediaArray.count;
}

// Creates each time a new cell if cell still doesn't have cached images
// it downloads images only when user stops at certain row
// sets delegate to update view when download is complete
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IFMedia* media = [_mediaArray objectAtIndex:indexPath.row];
    
    IFPhotoTableViewCell* cell = [[[NSBundle mainBundle] loadNibNamed:@"IFPhotoTableViewCellPortraitView" owner:self options:nil] objectAtIndex:0];
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"IFPhotoTableViewCellLandscapeView" owner:self options:nil] objectAtIndex:0];
    }
    [self setCachedContent:media];
    [cell setMedia:media];
    cell.delegate = self;
    
    if (![media.mediaFiles objectForKey:media.thumbnailURL]) {
        if (self.tableView.dragging == NO && self.tableView.decelerating == NO) {
            [self downloadCellContentForURL:media.thumbnailURL withMedia:media];
        }
    }
    if (![media.mediaFiles objectForKey:media.profileURL]) {
        if (self.tableView.dragging == NO && self.tableView.decelerating == NO) {
            [self downloadCellContentForURL:media.profileURL withMedia:media];
        }
    }
    return cell;
}

#pragma mark InstagramFeeding
// Sends request to retrieve user's feed as much as it is possible
// for each request there is a pagination info and instagram api allows
// to retrieve feed for a certain point

// All received media are stored in mutable dictionary
// to differentiate between liked media and daily feed
- (void) loadSelfFeed {
    _feedLoader = FeedLoaderInit;
    // Retrieve instagram user's feed
    [[InstagramEngine sharedEngine] getSelfFeedWithCount:21 maxId:self.currentPaginationInfo.nextMaxId success:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        self.currentPaginationInfo = paginationInfo;
        
        // this validation checks for information retrieved
        // if pagination information is already cached/downloaded
        // app bypasses this and continues queued tasks
        if ([_nextMaxIdSet containsObject:self.currentPaginationInfo.nextMaxId] || !paginationInfo) {
            NSLog(@"duplicate nextMaxID is: %@", [paginationInfo nextMaxId]);
            return;
        }
        if (self.currentPaginationInfo.nextMaxId) {
            [_nextMaxIdSet addObject:self.currentPaginationInfo.nextMaxId];
        } else {
            NSLog(@"next max id is going to null");
        }
        // Retrieve user feed media
        if (_feedLoader == FeedLoaderGotLiked) {
            _feedLoader = FeedLoaderNull;
        } else {
            _feedLoader = FeedLoaderGotFeed;
        }
        
        [_user addFeedMedia:media];
        [self selfFeedRequestResult:nil];
    } failure:^(NSError *error) {
        [self selfFeedRequestResult:error];
    }];
    [[InstagramEngine sharedEngine] getMediaLikedBySelfWithCount:21 maxId:self.currentPaginationInfo.nextMaxId success:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        // Retrieve user liked media
        [_user addLikedMedia:media];
        [self selfFeedRequestResult:nil];
    } failure:^(NSError *error) {
        [self selfFeedRequestResult:error];
    }];
}

// Update tableviewcell count on request received event
- (void)selfFeedRequestResult: (NSError*) error {
    if (error) {
        [self showAlertForError:error];
        NSLog(@"Request Self Feed Failed");
    } else {
        _mediaArray = [_user getFeedMedia];
        [self.tableView reloadData];
        if (_feedLoader == FeedLoaderGotFeed) {
            NSLog(@"got feed media, next maxID: %@", self.currentPaginationInfo.nextMaxId);
        }
    }
}

// Call this method if general errors are faced
- (void)showAlertForError:(NSError *) error {
    NSString* errorMessage = @"";
    
    if(NSURLErrorTimedOut == error.code) {
        errorMessage = @"Seems server is unreachable.";
    } else if(NSURLErrorNotConnectedToInternet == error.code) {
        errorMessage = @"Please check your internet connection.";
    } else if(NSURLErrorUnknown == error.code) {
        errorMessage = @"Something went wrong.";
    } else if (error.code == NSURLErrorBadServerResponse) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];

}

#pragma mark - UIInterfaceOrientationRotationEvents

// Reposition UI controls and scroll to last visible position before orientation changed
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    NSArray* visibleRows = [self.tableView indexPathsForVisibleRows];
    if (visibleRows.count == 0) {
        return;
    }
    _visibleCellIndexPath = [visibleRows objectAtIndex:visibleRows.count / 2];
    NSLog(@"Before %@", _visibleCellIndexPath);
}

// Update user interface according to orientation for visible rows
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    NSLog(@"After %@", _visibleCellIndexPath);
    [self.tableView scrollToRowAtIndexPath:_visibleCellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [self loadImagesForOnscreenRows];
}

#pragma mark ImageDownloading method

// Look through already downloaded data in cache
- (void)setCachedContent: (IFMedia*) media {
    if (![media.mediaFiles objectForKey:media.profileURL]) {
        [self cachedCellContentForURL:media.profileURL withMedia:media];
    }
    if (![media.mediaFiles objectForKey:media.thumbnailURL]) {
        [self cachedCellContentForURL:media.thumbnailURL withMedia:media];
    }
}

// Get image for specific URL
- (void)cachedCellContentForURL: (NSURL*) key withMedia: (IFMedia*) media {
    IFMediaDownloadManager* downloadManager = [IFMediaDownloadManager instance];
    UIImage* image = [downloadManager getDataFromCacheForURL:key];
    if (image) {
        [media.mediaFiles setObject:image forKey:key];
    }
}

- (void)startImageDownloadForMedia: (IFMedia*) media {
    if (![media.mediaFiles objectForKey:media.thumbnailURL]) {
        [self downloadCellContentForURL:media.thumbnailURL withMedia:media];
    }
    if (![media.mediaFiles objectForKey:media.profileURL]) {
        [self downloadCellContentForURL:media.profileURL withMedia:media];
    }
}

// Get cached image if available or start download
- (void)downloadCellContentForURL: (NSURL*) key withMedia: (IFMedia*) media {
    IFMediaDownloadManager* downloadManager = [IFMediaDownloadManager instance];
    [downloadManager addDownload:key withCompletionBlock:^(UIImage *image, NSURL *key, NSError *error) {
        [media.mediaFiles setObject:image forKey:key];
        [media.delegate loadedImageWithError:error];
    }];
}

// Gets array of indexPaths for visible rows
- (void)loadImagesForOnscreenRows {
    @synchronized(self.tableView) {
        if ([_mediaArray count] > 0) {
            NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
            for (NSIndexPath *indexPath in visiblePaths) {
                IFPhotoTableViewCell* cell = (IFPhotoTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
                if (cell) {
                    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            }
        }
    }
    
}

#pragma mark - UIScrollViewDelegate

// Download images for visible rows
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self loadImagesForOnscreenRows];
}

#pragma mark IFPhotoTableViewCellDelegate
- (void)likedMedia:(IFMedia *)media withError:(NSError *)error {
    if (error) {
        [self showAlertForError:error];
    }
}

- (void)updateCellContent:(IFPhotoTableViewCell *)cell withError:(NSError *)error {
    if (error) {
        [self showAlertForError:error];
    } else {
        NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
        if (!indexPath) {
            return;
        }
        NSArray* arr = @[indexPath];
        @try {
            [self.tableView reloadRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationNone];
        }
        @catch (NSException *exception) {
            NSLog(@"***********Throwed an exception: %@", [exception description]);
        }
        
    }
}

@end
