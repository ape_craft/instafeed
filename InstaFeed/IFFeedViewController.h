//
//  IFFeedViewController.h
//  InstaFeed
//
//  Created by Admin on 5/1/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramKit.h"

typedef NS_ENUM(NSInteger, FeedLoaderState) {
    FeedLoaderNull,
    FeedLoaderInit,
    FeedLoaderGotFeed,
    FeedLoaderGotLiked
};

@interface IFFeedViewController : UIViewController

@end
