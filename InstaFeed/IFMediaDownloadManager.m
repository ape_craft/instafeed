//
//  IFMediaDownloadManager.m
//  InstaFeed
//
//  Created by Admin on 5/2/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "IFMediaDownloadManager.h"
#import "IFMediaDownloader.h"

@interface IFMediaDownloadManager()

@property (strong, nonatomic) NSMutableDictionary *cachedContent;
@property (strong, nonatomic) NSMutableDictionary *activeDownloads;
@property (strong, nonatomic) IFMediaDownloadManager *instance;

@end

@implementation IFMediaDownloadManager

+ (instancetype)instance {
    static IFMediaDownloadManager *downloadManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        downloadManager = [[self alloc] init];
    });
    return downloadManager;
}

- (id)init {
    self = [super init];
    _cachedContent = [[NSMutableDictionary alloc] init];
    _activeDownloads = [[NSMutableDictionary alloc] init];
    return self;
}

-(UIImage *) getDataFromCacheForURL: (NSURL *) url {
    return [_cachedContent objectForKey:url];
}

// Start download of image if it is not cached in memory
-(void) addDownload:(NSURL *)url withCompletionBlock:(void (^)(UIImage *, NSURL *, NSError *))downloadCompletion {
    if (![_cachedContent objectForKey:url]) {
        IFMediaDownloader *mediaDownloader = [_activeDownloads objectForKey:url];
        if (!mediaDownloader) {
            NSString *temp = [NSString stringWithFormat:@"Starting download for url: %@", url];
            NSLog(@"%@", temp);
            mediaDownloader = [[IFMediaDownloader alloc] init];
            [_activeDownloads setObject:mediaDownloader forKey:url];
            [mediaDownloader startDownload:url completionBlock:^(UIImage *image, NSURL *key, NSError *error) {
                if (!error) {
                    [_cachedContent setObject:image forKey:key];
                    [_activeDownloads removeObjectForKey:key];
                    if (downloadCompletion) {
                        downloadCompletion(image, key, nil);
                    }
                } else {
                    if (downloadCompletion) {
                        downloadCompletion(nil, nil, error);
                    }
                }
            }];
        }
    } else {
        if (downloadCompletion) {
            downloadCompletion([_cachedContent objectForKey:url], url, nil);
        }
    }
}

-(void) cancelDownloadForURL:(NSURL *)url {
    IFMediaDownloader *currentDownload = [_activeDownloads objectForKey:url];
    [currentDownload stopDownload];
}

-(void) cancelAllDownloads {
    NSArray *allDownloads = [_activeDownloads allValues];
    [allDownloads makeObjectsPerformSelector:@selector(stopDownload)];
    [_activeDownloads removeAllObjects];
}

@end
