//
//  IFPhotoTableViewCell.h
//  InstaFeed
//
//  Created by Admin on 5/1/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IFMedia.h"

@class IFPhotoTableViewCell;

// Protocol for media liked/unliked event callback with error handling
@protocol IFPhotoTableViewCellDelegate <NSObject>

- (void) likedMedia: (IFMedia *) media withError:(NSError *) error ;
- (void) updateCellContent: (IFPhotoTableViewCell *) cell withError: (NSError *) error;

@end

@interface IFPhotoTableViewCell : UITableViewCell

@property (strong, nonatomic) id<IFPhotoTableViewCellDelegate> delegate;

- (void)setMedia: (IFMedia *) media;

@end
