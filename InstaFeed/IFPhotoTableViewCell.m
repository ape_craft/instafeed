//
//  IFPhotoTableViewCell.m
//  InstaFeed
//
//  Created by Admin on 5/1/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "IFPhotoTableViewCell.h"
#import "InstagramEngine.h"
#import "IFMediaDownloadManager.h"

@interface IFPhotoTableViewCell() <IFMediaImagesLoadDelegate>
@property (strong, nonatomic) IBOutlet UIImageView* photoImageView;
@property (strong, nonatomic) IBOutlet UIImageView* userProfileImageView;
@property (strong, nonatomic) IBOutlet UILabel* usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel* mediaCommentLabel;
@property (strong, nonatomic) IBOutlet UILabel* likesCountLabel;
@property (strong, nonatomic) IBOutlet UILabel* commentsCountLabel;
@property (strong, nonatomic) IBOutlet UILabel* postedDateLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView* profileImageIndicator;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView* mediaImageIndicator;
@property (strong, nonatomic) UITapGestureRecognizer* tap;

@property (strong, nonatomic) NSArray* commentsArray;
@property (strong, nonatomic) IFMedia* media;

@end

@implementation IFPhotoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark CustomMethodImplementations

// Set default media for cell
// and update subviews from data recieved
- (void)setMedia:(IFMedia *)media {
    _media = media;
    _media.delegate = self;
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mediaLikeAction:)];
    tap.numberOfTapsRequired = 2;
    tap.numberOfTouchesRequired = 1;
    [_photoImageView addGestureRecognizer:tap];
    [self updateCellView];
}

- (void)setLabelCaptions {
    [_usernameLabel setText:_media.username];
    [_mediaCommentLabel setText:_media.comment];
    [_likesCountLabel setText:[_media getLikesCount]];
    [_commentsCountLabel setText:[_media getCommentsCount]];
    [_postedDateLabel setText:_media.createdDate];
}

// Sets default image if no image is available
- (void)updateCellView {
    [self setLabelCaptions];
    if (![_media.mediaFiles objectForKey:_media.profileURL]) {
        [_profileImageIndicator startAnimating];
        _userProfileImageView.image = [UIImage imageNamed:@"InstagramLarge.png"];
    } else {
        _userProfileImageView.image = [_media.mediaFiles objectForKey:_media.profileURL];
        [_mediaImageIndicator stopAnimating];
    }
    if (![_media.mediaFiles objectForKey:_media.thumbnailURL]) {
        [_mediaImageIndicator startAnimating];
        _photoImageView.image = [UIImage imageNamed:@"InstagramLarge.png"];
    } else {
        _photoImageView.image = [_media.mediaFiles objectForKey:_media.thumbnailURL];
        [_profileImageIndicator stopAnimating];
    }
}

// Send like request to server on user double tap event
- (void)mediaLikeAction:(UIGestureRecognizer*)gesture {
    _media.liked = !_media.liked;
    [_media updateLikedAction];
    if (_media.liked) {
        [[InstagramEngine sharedEngine] likeMedia:_media.media withSuccess:^{
            if (self.delegate) {
                [self.delegate likedMedia:_media withError:nil];
                [self updateCellView];
            }
            NSLog(@"Like Success");
        } failure:^(NSError *error) {
            if (self.delegate) {
                [self.delegate likedMedia:_media withError:error];
            }
            NSLog(@"Like Failure");
        }];
    } else {
        [[InstagramEngine sharedEngine] unlikeMedia:_media.media withSuccess:^{
            if (self.delegate) {
                [self.delegate likedMedia:_media withError:nil];
                [self updateCellView];
            }
            NSLog(@"UNLike Success");
        } failure:^(NSError *error) {
            if (self.delegate) {
                [self.delegate likedMedia:_media withError:error];
            }
            NSLog(@"UNLike Failure");
        }];
    }
}

#pragma mark - IFMediaImagesLoadDelegate

// if something is went wrong this method is called with error parameter
// otherwise current cell is updated from superview
- (void)loadedImageWithError:(NSError *)error {
    [self updateCellView];
    if (_delegate) {
        [_delegate updateCellContent:self withError:error];
    }
}

@end
