//
//  IFUser.m
//  InstaFeed
//
//  Created by Admin on 5/3/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "IFUser.h"
#import "IFMedia.h"

@interface IFUser()

@property (strong, nonatomic) NSMutableDictionary *recentMedia;
@property (strong, nonatomic) NSMutableArray *likedMedia;
@property (strong, nonatomic) NSMutableArray *feedMedia;

@end

@implementation IFUser

- (id)init {
    self = [super init];
    _recentMedia = [[NSMutableDictionary alloc] init];
    _likedMedia = [[NSMutableArray alloc] init];
    _feedMedia = [[NSMutableArray alloc] init];
    return self;
}

- (void)addFeedMedia:(NSArray *)media {
    for (InstagramMedia *item in media) {
        if ([_recentMedia objectForKey:item.lowResolutionImageURL]) {
            [_feedMedia addObject:[_recentMedia objectForKey:item.lowResolutionImageURL]];
            continue;
        }
        IFMedia *temp = [[IFMedia alloc] initWithInstagramMedia:item liked:NO];
        [_feedMedia addObject:temp];
        [_recentMedia setObject:temp forKey:item.lowResolutionImageURL];
    }
}

- (void)addLikedMedia:(NSArray *)media {
    for (InstagramMedia *item in media) {
        NSURL *url = item.lowResolutionImageURL;
        IFMedia *temp1 = [_recentMedia objectForKey:url];
        if(temp1) {
            temp1.liked = YES;
            [_likedMedia addObject:temp1];
            continue;
        }
        IFMedia *temp = [[IFMedia alloc] initWithInstagramMedia:item liked:YES];
        [_likedMedia addObject:temp];
        [_recentMedia setObject:temp forKey:url];
    }
}

- (NSMutableArray *)getFeedMedia {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (IFMedia* media in _feedMedia) {
        [result addObject:[_recentMedia objectForKey:media.thumbnailURL]];
    }
    return result;
}

- (NSMutableArray *)getLikedMedia {
    return _likedMedia;
}

@end
