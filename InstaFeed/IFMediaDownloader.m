//
//  IFMediaDownloader.m
//  InstaFeed
//
//  Created by Admin on 5/2/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "IFMediaDownloader.h"

@interface IFMediaDownloader() <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, strong) NSMutableData *activeDownload;
@property (nonatomic, strong) NSURLConnection* imageConnection;

@end;

@implementation IFMediaDownloader

-(void) startDownload:(NSURL *)url completionBlock:(void (^)(UIImage *, NSURL *, NSError *))completionBlock {
    self.activeDownload = [NSMutableData data];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.completionHandler = completionBlock;
    self.imageConnection = conn;
    [self.imageConnection start];
}

-(void) stopDownload {
    [self.imageConnection cancel];
    self.imageConnection = nil;
    self.activeDownload = nil;
}

#pragma mark - NSURLConnectionDataDelegate
-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.activeDownload appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if (self.completionHandler) {
        self.completionHandler(nil, [[connection currentRequest] URL], error);
    }
    self.activeDownload = nil;
    self.imageConnection = nil;
}

-(void) connectionDidFinishLoading:(NSURLConnection *)connection {
    UIImage *image = [[UIImage alloc] initWithData:self.activeDownload];
    self.activeDownload = nil;
    self.imageConnection = nil;
    if (self.completionHandler) {
        self.completionHandler(image, [[connection currentRequest] URL], nil);
    }
}

@end
