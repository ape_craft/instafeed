//
//  IFMedia.m
//  InstaFeed
//
//  Created by Admin on 5/2/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "IFMedia.h"

@implementation IFMedia

-(id) initWithInstagramMedia:(InstagramMedia *)media liked:(BOOL)liked {
    self = [super init];
    self.thumbnailURL = media.lowResolutionImageURL;
    self.profileURL = media.user.profilePictureURL;
    self.media = media;
    self.username = [NSString stringWithFormat:@"💬 %@", media.user.username];
    self.comment = media.caption.text;
    self.likesCount = media.likesCount;
    self.liked = liked;
    self.commentsCount = media.commentCount;
    
    double interval = [media.createdDate timeIntervalSinceNow] * -1;
    NSString *diff = @"";
    
    //Customize media created date for UI
    if (interval < 60) {
        diff = [NSString stringWithFormat:@"%d s ago", (int)interval];
    } else if (interval < 3600){
        diff = [NSString stringWithFormat:@"%d m ago", (int)interval / 60];
    } else if (interval < 86400){
        diff = [NSString stringWithFormat:@"%d h ago", (int)interval / 3600];
    } else if (interval < 604800){
        diff = [NSString stringWithFormat:@"%d d ago", (int)interval / 86400];
    } else if (interval < 31449600){
        diff = [NSString stringWithFormat:@"%d w ago", (int)interval / 604800];
    } else {
        diff = [NSString stringWithFormat:@"%d y ago", (int)interval / 31449600];
    }
    self.createdDate = [NSString stringWithFormat:@"📅 %@", diff];

    self.mediaFiles = [[NSMutableDictionary alloc] init];
    
    return self;
}

-(void) updateLikedAction {
    if (self.liked) {
        self.likesCount++;
    } else {
        self.likesCount--;
    }
}

-(NSString *) getLikesCount {
    if (self.likesCount == 0) {
        return @"";
    } else {
        if (self.liked) {
            return [NSString stringWithFormat:@"♥ %ld", (long)self.likesCount];
        } else {
            return [NSString stringWithFormat:@"♡ %ld", (long)self.likesCount];
        }
    }
}

-(NSString *) getCommentsCount {
    if (self.commentsCount == 0) {
        return @"";
    } else {
        return [NSString stringWithFormat:@"%ld 💬", (long)self.commentsCount];
    }
}

@end
