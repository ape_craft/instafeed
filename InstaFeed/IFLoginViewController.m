//
//  IFLoginViewController.m
//  InstaFeed
//
//  Created by Admin on 5/1/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "IFLoginViewController.h"
#import "InstagramKit.h"

@interface IFLoginViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (assign, nonatomic) BOOL restoredToken;

@end

@implementation IFLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// If there is token is stored reuse it if on
// request gets error -1011 pops view controller then clears all cached
// stored datas and requires user's credentials
- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [[InstagramEngine sharedEngine] setAccessToken:[defaults objectForKey:@"ACCESS TOKEN"]];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _webView.scrollView.bounces = NO;
    _webView.contentMode = UIViewContentModeScaleAspectFit;
    _webView.delegate = self;
    if ([[InstagramEngine sharedEngine] accessToken]) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"nsinstafeed:/access_token=%@", [[InstagramEngine sharedEngine] accessToken]]];
        _restoredToken = YES;
        [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    } else {
        
        NSDictionary *configuration = [InstagramEngine sharedEngineConfiguration];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments", configuration[kInstagramKitAuthorizationUrlConfigurationKey], configuration[kInstagramKitAppClientIdConfigurationKey], configuration[kInstagramKitAppRedirectUrlConfigurationKey]]];
        [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

// If self feed request retrieval is failed it may be because of expired token
// here is token updated on bad response is recieved from server
// and cleans up all cookies and caches
- (void)viewWillAppear:(BOOL)animated {
    if (!_restoredToken) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSDictionary *configuration = [InstagramEngine sharedEngineConfiguration];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments", configuration[kInstagramKitAuthorizationUrlConfigurationKey], configuration[kInstagramKitAppClientIdConfigurationKey], configuration[kInstagramKitAppRedirectUrlConfigurationKey]]];
        [[NSURLCache sharedURLCache] removeCachedResponseForRequest:[NSURLRequest requestWithURL:url]];
        
        for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            
            if([[cookie domain] rangeOfString:@"instagram.com"].location != NSNotFound) {
                
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            }
        }
        
        [_webView loadRequest:[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *URLString = [request.URL absoluteString];
    if ([URLString hasPrefix:[[InstagramEngine sharedEngine] appRedirectURL]]) {
        NSString *delimiter = @"access_token=";
        NSArray *components = [URLString componentsSeparatedByString:delimiter];
        if (components.count > 1) {
            NSString *accessToken = [components lastObject];
            NSLog(@"ACCESS TOKEN = %@",accessToken);
            [[InstagramEngine sharedEngine] setAccessToken:accessToken];
            [self performSegueWithIdentifier:@"IFLoggedIn" sender:nil];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:accessToken forKey:@"ACCESS TOKEN"];
            [defaults synchronize];
        }
        _restoredToken = NO;
        return NO;
    }
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSString *errorMessage = @"";
    switch (error.code) {
        case NSURLErrorTimedOut:
            errorMessage = @"Server is unreachable, please try again later.";
            break;
        case NSURLErrorCancelled:
            errorMessage = @"Other side rejected.";
            break;
        case NSURLErrorNotConnectedToInternet:
            errorMessage = @"Please check your internet connection.";
            break;
        default:
            errorMessage = @"Something went wrong :(";
            break;
    }
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

@end
