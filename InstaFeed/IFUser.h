//
//  IFUser.h
//  InstaFeed
//
//  Created by Admin on 5/3/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstagramMedia.h"

@interface IFUser : NSObject

- (void) addLikedMedia: (NSArray *) media;
- (void) addFeedMedia: (NSArray *) media;
- (NSMutableArray *) getLikedMedia;
- (NSMutableArray *) getFeedMedia;

@end
