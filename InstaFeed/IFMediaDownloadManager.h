//
//  IFMediaDownloadManager.h
//  InstaFeed
//
//  Created by Admin on 5/2/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
// Stores all downloaded images and manages download queues
@interface IFMediaDownloadManager : NSObject

+ (instancetype) instance;
- (void) addDownload: (NSURL *) url withCompletionBlock: (void (^)(UIImage *image, NSURL *key, NSError *error))downloadCompletion;
- (void) cancelDownloadForURL: (NSURL *) url;
- (void) cancelAllDownloads;
- (UIImage *) getDataFromCacheForURL: (NSURL *) url;

@end
