//
//  IFMediaDownloader.h
//  InstaFeed
//
//  Created by Admin on 5/2/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IFMedia.h"

@interface IFMediaDownloader : NSObject

@property (strong, nonatomic) void (^completionHandler)(UIImage *image, NSURL *key, NSError *error);

- (void) startDownload: (NSURL *) url completionBlock: (void (^)(UIImage *image, NSURL *key, NSError *error))completionBlock;
- (void) stopDownload;

@end
